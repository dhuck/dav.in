# section information
section:
  name: Projects / CV
  id: projects
  enable: true
  weight: 4
  showOnNavbar: true
  # Can optionally hide the title in sections
  # hideTitle: true

# filter buttons
buttons:
  - name: All
    filter: "all"
  - name: Professional
    filter: "professional"
  - name: Academic
    filter: "academic"
  - name: Hobby
    filter: "hobby"

# your projects
projects:
  - name: Great Gold Bird
    role: Technical Director
    timeline: Summer 2024
    logo: "/images/sections/projects/briefcase-solid.svg"
    summary: Technical Directory for a wanderplay in Los Angeles, CA. Responsible for all technical aspects of the show, including web development, raspberry pi integration, and custom SIP media phone menus.
    url: "https://greatgoldbird.com"
    tags:
      [
        "professional", 
        "web development", 
        "raspberry pi", 
        "aws"
      ]
  - name: Master's Thesis
    role: Solo Researcher
    timeline: Fall 2023 - Spring 2024
    logo: "/images/sections/projects/school-solid.svg"
    summary: Studied approaches to AI image generation based on musical and textual inputs. First approach is lyric-based, producing image-generation prompts using LLMs via a synthesis of textual analysis and summarization of lyrics. Second approach fine tunes a MIR model to describe images which correspond to provided musical snippets. Final approach fine-tunes a Stable Diffusion XL model to directly produce images from audio embeddings provided by AST. Results show that the lyric-based approach is the most successful, while other approaches are highly data-constrained.
    url: /pdf/masterthesis.pdf
    tags:
      [
        "academic",
        "pytorch",
        "nlp",
        "music information retrieval",
        "generative ai",
      ]

  - name: Yet Another Computer Go
    role: Project Contributor
    timeline: Spring 2024
    logo: "/images/sections/projects/school-solid.svg"
    summary: Semester project implementing core concepts from AlphaGo and AlphaGoZero to train a Go playing agent. Major accomplishment was using ZeroMQ and small networks to colocate multiple inference instances on a single consumer-grade GPU.
    tags: ["academic", "pytorch", "alphago", "reinforcement learning", "AI"]
    url: "https://github.com/d-huck/yacgo"

  - name: General Purpose Audio Embeddings
    role: Project Leader/Contributor
    timeline: Fall 2023
    logo: "/images/sections/projects/school-solid.svg"
    summary: Semester project studying the applicability of Encodec towards producing general purpose audio embeddings. Our approach encodes AudioSet using Encodec and then trains MobileNetV3 to classify the original AudioSet tags. We evaluate the performance using linear probing and find that while the approach underperforms in specific tasks, it performs surprisingly consistently across all tasks.
    url: /pdf/gpae_encodec.pdf
    tags: ["academic", "pytorch", "audio embeddings", "encodec", "mobilenet"]

  - name: Fabman API
    role: Lead Developer
    timeline: Jun 2023 - Aug 2023
    logo: "/images/sections/projects/briefcase-solid.svg"
    summary: Python library for interacting with Fabman.io API for maker space access control. Developed to transition TIW maker space from homespun solution to Fabman.io.
    repo: https://github.com/utexas-engr-tiw/fabman-api
    tags: ["professional", "python", "API"]

  - name: Synthesis Synthesis
    role: Sole Contributor
    timeline: "March 2023 - May 2023"
    logo: "/images/sections/projects/school-solid.svg"
    url: /pdf/synthesis-synthesis.pdf
    # repo: https://github.com/kubernetes/kubernetes # If your project is a public repo on GitHub, then provide this link. it will show star count.
    #url: ""  # If your project is not a public repo but it has a website or any external details url then provide it here. don't provide "repo" and "url" simultaneously.
    summary: Using LLM to synthesis Faust code to build digital synthesizers. Based loosely on AlphaCode, a LLM produces novel Faust code to build new audio synthesizer architectures.
    tags: ["academic", "pytorch", "nlp", "program synthesis"]

  - name: Efficient Dataloading for Visiomotor Control
    role: Lead Contributor
    url: /pdf/improving-dataloaders.pdf
    logo: "/images/sections/projects/school-solid.svg"
    timeline: Sep 2022 - Dec 2022
    summary: Final Project for Robot Learning graduate course. Comparison of different torch data strategies resulting in 33% more efficient data loading for large multi-modal datasets.
    tags: ["academic", "pytorch", "dataloading", "robomimic"]

  - name: Celestial Object Classification
    role: Sole Contributor
    logo: "/images/sections/projects/school-solid.svg"
    timeline: Nov 2022 - Dec 2022
    summary: Final project for undergraduate ML intro course. Used machine learning to dig into the Gaia Data Release version 3 to identify celestial objects.
    tags: ["academic", "sklearn", "machine learning"]

  - name: Good Project, Sir
    role: Sole Contributor
    logo: "/images/sections/projects/school-solid.svg"
    url: /pdf/good-project-sir.pdf
    timeline: Mar 2022 - May 2022
    summary: Using few-shot learning and Pattern-Exploitative Training to identify spam and sentiment at the heart of the 2022 crypto boom and bust.
    tags: ["academic", "pytorch", "nlp", "sentiment analysis"]

  - name: Quantum Computing Simulator
    role: Sole Contributor
    logo: "/images/sections/projects/school-solid.svg"
    timeline: Apr 2022 - May 2022
    summary: Using python and numpy to simulate gates for quantum computing calculations.
    tags: ["academic", "numpy", "python"]

  - name: Texas Inventionworks Portal
    role: Lead Developer
    logo: "/images/sections/projects/briefcase-solid.svg"
    timeline: Jun 2021 - Jun 2023
    summary: Web Portal application for Texas Inventionworks, the makerspace located in the heart of the Engineering Education Research building at UT Austin.
    tags: ["professional", "React", "AWS"]

  - name: libdigitone
    role: Sole Developer
    timeline: Aug 2019 - Oct 2019
    logo: "/images/sections/projects/person-rays-solid.svg"
    repo: https://github.com/d-huck/libdigitone
    summary: Python library for parsing SysEx information from the Elektron Digitone. No longer maintained to parse newer firmware.
    # url: /posts/personal/libdigitone
    tags: ["hobby", "MIDI", "Music"]
# - name: Tensorflow
#   logo: /images/sections/projects/tensorflow.png
#   role: Developer
#   timeline: "Jun 2018 - Present"
#   repo: https://github.com/tensorflow/tensorflow
#   #url: ""
#   summary: An Open Source Machine Learning Framework for Everyone.
#   tags: ["professional", "machine-learning"]

# - name: A sample academic paper
#   role: Team Lead
#   timeline: "Jan 2017 - Nov 2017"
#   url: "https://www.example.com"
#   summary: Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente eius reprehenderit animi suscipit autem eligendi esse amet aliquid error eum. Accusantium distinctio soluta aliquid quas placeat modi suscipit eligendi nisi.
#   tags: ["academic","iot"]

# - name: Nocode
#   logo: /images/sections/projects/no-code.png#   role: Nothing
#   timeline: "Oct 2019 - Dec 2019"
#   repo: https://github.com/kelseyhightower/nocode
#   #url: ""
#   summary: The best way to write secure and reliable applications. Write nothing; deploy nowhere.
#   tags: ["hobby", "fun"]

# - name: Toha
#   logo: /images/sections/projects/toha.png
#   role: Owner
#   timeline: "Jun 2019 - Present"
#   repo: https://github.com/hossainemruz/toha
#   summary: A Hugo theme for personal portfolio.
#   tags: ["hobby","hugo","theme","professional"]
