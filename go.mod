module github.com/hugo-toha/hugo-toha.github.io

go 1.21

toolchain go1.22.5

require github.com/hugo-toha/toha/v4 v4.6.0 // indirect
