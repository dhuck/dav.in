---
title: "libdigitone"
date: 2019-02-11T22:53:58+05:30
description: Open-source SysEx parsing library for programmatic interfacing with the Elektron Digitone.
draft: false
menu:
  sidebar:
    name: libdigitone
    parent: personal
    identifier: libdigitone
    weight: 100
hero: libdigitone.png
tags: ["MIDI", "Audio Synthesis", "Python"]
categories: ["Personal Projects"]
---
libDigitone is a portable SysEx library written in python for the Elektron
Digitone. This library was started before the Digitone was added to the
Overbridge family of devices. You can find the source code
[here](https://gitlab.com/dhuck/libdigitone).

Future plans include translating the library into C and Swift to allow an easier
integration with desktop computers and Apple devices.